package main;

public interface RateLimiter {

    boolean grantAccess();
}
