package main.leakybucket;

import main.RateLimiter;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class LeakyBucket implements RateLimiter {

    BlockingQueue<Integer> queue;

    public LeakyBucket(int capacity){
        queue = new LinkedBlockingQueue<>(capacity);
    }
    @Override
    public boolean grantAccess() {

        // not a thread sage operation
        if(queue.remainingCapacity() > 0){
            queue.add(1);
            return true;
        }
        return false;
    }
}
