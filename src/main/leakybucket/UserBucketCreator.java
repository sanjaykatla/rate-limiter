package main.leakybucket;

import java.util.HashMap;
import java.util.Map;

public class UserBucketCreator {

    Map<Integer, LeakyBucket> map;

    public UserBucketCreator() {
        map = new HashMap<>();
    }

    void accessApplication(int userId) {
        if (map.get(userId) == null) {
            // not a thread sage operation
            map.put(userId, new LeakyBucket(10));
        }

        boolean grantAccess = map.get(userId).grantAccess();
        if (grantAccess) {
            System.out.println(Thread.currentThread().getName() + " Granted");
        } else {
            System.out.println(Thread.currentThread().getName() + " Access not Granted");
        }
    }
}
