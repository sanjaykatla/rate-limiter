package main.tokenbucket;

import main.RateLimiter;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class TokenBucket implements RateLimiter {

    int bucketCapacity;
    int refreshRate;
    AtomicInteger current;
    AtomicLong lastUpdatedTime;
    public TokenBucket(int bucketCapacity, int refreshRate) {
        this.bucketCapacity = bucketCapacity;
        this.refreshRate = refreshRate;
        current = new AtomicInteger(bucketCapacity);
        lastUpdatedTime = new AtomicLong(System.currentTimeMillis());
    }
    @Override
    public boolean grantAccess() {

        refreshBucket();
        if(current.get() > 0){
            current.decrementAndGet();
            return true;
        }
        return false;
    }

    private void refreshBucket(){
        long currentTime = System.currentTimeMillis();
        int additionalToken = (int) ((currentTime-lastUpdatedTime.get())/1000 * refreshRate);
        int currCapacity = Math.min(current.get()+additionalToken, bucketCapacity);
        current.getAndSet(currCapacity);
        lastUpdatedTime.getAndSet(currentTime);
    }
}
