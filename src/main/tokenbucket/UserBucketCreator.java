package main.tokenbucket;

import java.util.HashMap;
import java.util.Map;

public class UserBucketCreator {

    Map<Integer, TokenBucket> bucket;
    public UserBucketCreator(){

        bucket = new HashMap<>();
    }

    void accessApplication(int id){

        if(!bucket.containsKey(id)){
            bucket.put(id, new TokenBucket(10,10));
        }
        if(bucket.get(id).grantAccess()){
            System.out.println(Thread.currentThread().getName() + " -> able to access the application");
        }else{
            System.out.println(Thread.currentThread().getName() + " -> Too many request, Please try after some time");
        }
    }
}
